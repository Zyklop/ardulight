class User(object):
    """description of class"""

    def __init__(self, name, token, role, id) :
        self.id = id
        self.name = name
        self.token = token
        self.role = role
        self.isAdmin = (role == 1)
        self.canWrite = (role == 2)
        self.canRead = (role == 3)


