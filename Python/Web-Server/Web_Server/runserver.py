"""
This script runs the Web_Server application using a development server.
"""
import os, select, socket
from flask import Flask
from celery import Celery
from tasks import BroadcastParser

def make_celery(app):
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery

parser = BroadcastParser()

app = Flask(__name__)
app.config.update(
    REDIS_ENABLED = False,
    REDIS_URL = "",
    REDIS_DATABASE = 0,
    BROKER = 'pyamqp://yun@localhost//'
)
celery = make_celery(app)

from controller.HomeController import homePage
from controller.UserController import userPage

app.register_blueprint(homePage)
app.register_blueprint(userPage)

@celery.task()
def listen_to_udp():
    listener = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_UDP)
    listener.bind(('0.0.0.0', 1337))
    while True:
        r, w, x = select.select([listener], [], [])
        for i in r:
            parser.ParseMessage(i.recvfrom(131072))

@celery.task()
def announce():
    mcsocket = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_UDP)
    mcsocket.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
    mcsocket.sendto('{"type":"announcment", "hostname":"' + socket.gethostname() + '"}', ('224.3.2.1', 1337))

if __name__ == '__main__':
    HOST = 'localhost'
    app.run(HOST, 8080)
    listen_to_udp().task()

