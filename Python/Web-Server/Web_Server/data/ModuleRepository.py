import BaseRepository
import uuid
import json
from models.Module import Module

class ModuleRepository(BaseRepository.BaseRepository):
    def createAllTables(self) :
        columns = {"Name": "TEXT NOT NULL", "IP": "TEXT NOT NULL", 
                   "UID": "TEXT NOT NULL", "Latitude": "INTEGER NULL", 
                   "Longitude": "INTEGER NULL", "Height": "INTEGER NULL"}
        self.verifyTable("Module", columns)

    def __init__(self, logger) :
        BaseRepository.BaseRepository.__init__(self, logger)

    def getModuleFromRow(self, rowOrCursor):
        if (rowOrCursor is sqlite3.Cursor):
            row = cursor.fetchone()
        else:
            row = rowOrCursor
        if (row) :
            return Module(row[6], row[0], row[1], row[2], row[3], row[4], row[5])
        return None

    def getModuleById(self, id):
        cursor = self.query("SELECT Name, IP, UID, Latitude, Longitude, Height, rowid FROM Module WHERE rowid = ?", id)
        return self.getModuleFromRow(cursor)
    
    def getModuleByIp(self, ip):
        cursor = self.query("SELECT Name, IP, UID, Latitude, Longitude, Height, rowid FROM Module WHERE IP = ?", ip)
        return self.getModuleFromRow(cursor)
    
    def getModuleByUid(self, uid):
        cursor = self.query("SELECT Name, IP, UID, Latitude, Longitude, Height, rowid FROM Module WHERE UID = ?", uid)
        return self.getModuleFromRow(cursor)

    def getAllModules(self):
        cursor = self.query("SELECT Name, IP, UID, Latitude, Longitude, Height, rowid FROM Module")
        modules = []
        rows = cursor.fetchall()
        for row in rows:
            modules.append(getModuleFromRow(row))
        return modules

    def createModule(self, Ip, Uid, Latitude, Longitude, Height):
        token = str(newToken())
        self.execute("INSERT INTO Module (Name, IP, UID, Latitude, Longitude, Height, ) VALUES (?, ?, ?, ?, ?, ?)", Ip, Ip, Uid, Latitude, Longitude, Height)
        return ""

    def renameModule(self, id, name):
        self.query("UPDATE Module SET Name = ?, WHERE rowid = ?", name, id)
        return ""

    def exportModules(self):
        cursor = self.query("SELECT Name, IP, UID, Latitude, Longitude, Height, rowid FROM Module")
        return json.dump(cursor.fetchAll())

def getThisModule():
