import os
import sqlite3
import datetime

class BaseRepository():
    def verifyTable(self, name, columns) :
        q = "CREATE TABLE IF NOT EXISTS " + name + "(\n"
        first = True
        for key, value in columns.iteritems():
                if first != True :
                        q += ","
                first = False
                q += "\n" + key + " " + value
        q += "\n);"
        self.execute(q)
        return True

    def __init__(self, logger) :
        self.logger = logger
        if os.name == "nt" :
                self.dbPath = "C:/Temp/ardulight.db"
        else :
                self.dbPath = "/mnt/sda1/db/ardulight.db"
        self.createAllTables()
        
    def connect(self) :
            self.connection = sqlite3.connect(self.dbPath)

    def execute(self, query, params=None) :
        conn = sqlite3.connect(self.dbPath)
        self.logger.info("connected, executing" + query)
        if(params):
            conn.execute(query, params)
        else:
            conn.execute(query)
        conn.commit()

    def query(self, query, *params) :
        conn = sqlite3.connect(self.dbPath)
        self.logger.info("connected, querying" + query)
        if(params):
            cursor = conn.execute(query, params)
        else:
            cursor = conn.execute(query)
        conn.commit()
        return cursor

def getTimestamp():
    return str(datetime.datetime.utcnow().isoformat())
