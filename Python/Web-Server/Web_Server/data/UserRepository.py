import BaseRepository
import uuid
import json
from models.User import User

class UserRepository(BaseRepository.BaseRepository):
    def createAllTables(self) :
        columns = {"Name": "TEXT NOT NULL", "Password": "TEXT NOT NULL", "AccessToken": "TEXT NULL", "Role": "INT NOT NULL", "ChangeStamp": "TEXT NOT NULL"}
        self.verifyTable("User", columns)

    def __init__(self, logger) :
        BaseRepository.BaseRepository.__init__(self, logger)

    def getUser(self, request):
        if (request):
            token = request.cookies.get('token')
            if (token):
                cursor = self.query("SELECT Name, AccessToken, Role, rowid FROM User WHERE AccessToken = ?", token)
                row = cursor.fetchone()
                if (row) :
                    return User(row[0], row[1], row[2], row[3])
            username = request.form["Username"]
            password = request.form["Password"]
            user = userRepo.login(username, password)
            return user
        return None

    def updateUser(self, id, username, password, roleId):
        querystring = "UPDATE USER Set Name = ?, Password = ?, ChangeStamp = ?"
        if (roleId):
            querystring += ", Role = ?"
        querystring += " WHERE rowid = ?"
        if (roleId):
            self.execute(querystring, username, password, getTimestamp(), roleId, id)
        else:
            self.execute(querystring, username, password, getTimestamp(), id)
        return ""
    
    def createUser(self, id, username, password, roleId):
        cursor = self.query("SELECT 1 FROM User WHERE Name = '" + username + "'")
        if (cursor.fetchone()):
                return "Username aready in use"
        token = str(newToken())
        self.execute("INSERT INTO User (Name, Password, AccessToken, Role, ChangeStamp) VALUES (?, ?, ?, ?, ?)", username, password, token, str(roleId), getTimestamp())
        return ""

    def login(self, username, password):
        cursor = self.query("SELECT Name, AccessToken, Role, rowid FROM User WHERE Name = ? AND Password = ?", [username, password])
        row = cursor.fetchone()
        if (row):
            if (row[1]):
                user = User(row[0], row[1], row[2], row[3])
            else:
                token = newToken()
                user = User(row[0], token, row[2], row[3])
                self.query("UPDATE User SET AccessToken = ?, ChangeStamp = ? WHERE rowid = ?", token, getTimestamp(), user.id)
            return user
        return None

    def hasAdminUsers(self):
        cursor = self.query("SELECT count(*) FROM User WHERE Role = 1")
        row = cursor.fetchone()
        if (row[0] == 0):
               return False
        return True

    def getAllUsers(self):
        cursor = self.query("SELECT Name, AccessToken, Role, rowid FROM User")
        users = []
        rows = cursor.fetchall()
        for row in rows:
               users.append(User(row[0], row[1], row[2], row[3]))
        return users

    def getUserById(self, id):
        cursor = self.query("SELECT Name, AccessToken, Role, rowid FROM User WHERE rowid = ?", id)
        row = cursor.fetchone()
        if (row):
            if (row[1]):
                user = User(row[0], row[1], row[2], row[3])
            else:
                token = newToken()
                user = User(row[0], token, row[2], row[3])
                self.query("UPDATE User SET AccessToken = ?, ChangeStamp = ? WHERE rowid = ?", token, getTimestamp(), user.id)
            return user
        return None


    def exportUsers(self):
        cursor = self.query("SELECT rowid, Name, Password, AccessToken, Role, ChangeStamp FROM User")
        return json.dump(cursor.fetchAll())

def newToken():
    token = uuid.uuid4().hex
    return token