from flask import Blueprint, render_template, request, abort
from data.UserRepository import UserRepository
from data.ModuleRepository import ModuleRepository
import logging
import models.User

homePage = Blueprint("home_page", "Home", template_folder="templates")

UserRepo = UserRepository(logging.getLogger("ardulight"))
ModuleRepo = ModuleRepository(logging.getLogger("ardulight"))

@homePage.route("/")
@homePage.route("/index")
def index():
	user = UserRepo.getUser(request)
	return render_template("index.html", title= "Ardulight", user=user)
