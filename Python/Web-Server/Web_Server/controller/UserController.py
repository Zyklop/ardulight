from flask import Blueprint, render_template, request, make_response, redirect
from data.UserRepository import UserRepository
from models.User import User
import logging

userPage = Blueprint("user_page", "User", template_folder="templates")

userRepo = UserRepository(logging.getLogger("ardulight"))

@userPage.route("/login", methods=['GET', 'POST'])
def login():
	error = ""
	if (request.method == 'POST'):
		username = request.form["Username"]
		password = request.form["Password"]
		user = userRepo.login(username, password)
		if (user) :
			resp = make_response(redirect("overview"))
			resp.set_cookie('token', user.token)
			return resp
		else:
			error = "Username/Password invalid"
	return render_template("login.html", title = "Login", error = error)

@userPage.route("/admin", methods=['GET'])
def getAdminPage():
	if (userRepo.hasAdminUsers() == False):
		return render_template("createUser.html", role=1, title="Create User")
	user = userRepo.getUser(request)
	if (user.isAdmin):
		users = userRepo.getAllUsers()
		return render_template("userOverview.html", title = "User Management", users = users, user = user)
	else:
		return redirect("userEdit/" + user.id)
	return redirect("login")

@userPage.route("/admin", methods=['POST'])
def postAdminPage():
	if (userRepo.hasAdminUsers() == True):
		return render_template("createUser.html", role=1, title="Create User", error="Admin already set")
	username = request.form["Username"]
	password = request.form["Password"]
	if (request.form["RepeatPassword"] != password):
		return render_template("createUser.html", role=1, title="Create User", error="passwords doesn't match")
	res = userRepo.createUser(username, password, 1)
	if (res != ""):
		return render_template("createUser.html", role=1, title="Create User", error=res)
	return redirect("login")

@userPage.route("/editUser/<int:userId>", methods = ['GET', 'POST'])
def userEdit(userId):
    user = userRepo.getUser(request)
    editUser = userRepo.getUserById(userId)
    if (request.method == 'POST'):
        if (user.id == userId or user.isAdmin):
            username = request.form["Username"]
            password = request.form["Password"]
            if (request.form["RepeatPassword"] != password):
                return render_template("createUser.html", role=1, title="Create User", error="passwords doesn't match")
            role = request.form["Role"]
            if (not user.isAdmin):
                role = None
            userRepo.updateUser(userId, username, password, role)
        return redirect("index")
    if (user.id == userId or user.isAdmin):
        return render_template("editUser.html", title = "Edit User", user = user, roleEdit = user.isAdmin, editUser = editUser)
    return redirect("index")

@userPage.route("/createUser/<int:roleId>", methods = ['GET'])
def getCreateUser(roleId = None):
    user = userRepo.getUser(request)
    if (roleId < 2 or roleId > 3):
        roleId = 3
    return render_template("createUser.html", role=2, title = "Create User", user = user)

@userPage.route("/createUser", methods = ['POST'])
def createUser():
    if (roleId < 2 or roleId > 3):
        roleId = 3
    return render_template("createUser.html", role=2, title = "Create User")