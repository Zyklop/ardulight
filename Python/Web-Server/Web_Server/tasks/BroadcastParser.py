from data import ModuleRepository

class BroadcastParser():
    def __init__(self):
        self.moduleRepo = ModuleRepository(logging.getLogger("ardulight"))

    def ParseMessage(self, message, source):
        splitted = message.split(';')
        if ('announce' in message):
            self.moduleRepo.createModule(source, splitted[1], splitted[2], splitted[3], splitted[4]) 
