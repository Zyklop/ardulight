﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Schema;
using Communication.ApiCommunication;
using DataPersistance.Modules;
using Color = System.Windows.Media.Color;

namespace Proto
{
	/// <summary>
	/// Interaction logic for PresetSelector.xaml
	/// </summary>
	public partial class PresetSelector : Window
	{
		private LedController ledController;
		private List<MappedLed> leds;
		private double xMax;
		private double yMax;
		private Ellipse selection;

		public PresetSelector()
		{
			selection = new Ellipse
			{
				Width = 10.0,
				Height = 10.0,
				Fill = Brushes.Transparent,
				Stroke = Brushes.Red,
				StrokeThickness = 2.0
			};
			ledController = new LedController();
			leds = ledController.GetAllLeds().Select(x => new MappedLed{Led = x, Pixel = new Rectangle
				{
					Width = 2.0,
					Height = 2.0,
					Fill = ToSCB(x)
				}}).ToList();
			yMax = leds.Max(x => x.Led.Y);
			xMax = leds.Max(x => x.Led.X);

			InitializeComponent();

			Canvas.SetRight(selection, 0);
			Canvas.SetBottom(selection, 0);
			cvPoints.Children.Add(selection);
			leds.ForEach(x => cvPoints.Children.Add(x.Pixel));
			RenderLeds();
		}

		private static SolidColorBrush ToSCB(LedValue x)
		{
			return new SolidColorBrush(Color.FromRgb(x.Color.R, x.Color.G, x.Color.B));
		}

		private void RenderLeds()
		{
			foreach (var ledValue in leds)
			{
				ledValue.MappedPosition = new Point(ledValue.Led.X * XScaling, ledValue.Led.Y * YScaling);
				Canvas.SetTop(ledValue.Pixel, ledValue.MappedPosition.Y);
				Canvas.SetLeft(ledValue.Pixel, ledValue.MappedPosition.X);
			}
		}

		public double YScaling => cvPoints.ActualHeight / yMax;

		public double XScaling => cvPoints.ActualWidth / xMax;

		private void Save(object sender, RoutedEventArgs e)
		{
			ledController.SavePreset(txtName.Text, leds.Where(x => x.Led.Color.Active).Select(x => x.Led));
			Close();
		}

		private void ReRender(object sender, SizeChangedEventArgs e)
		{
			RenderLeds();
		}

		private void DrawSelectionCircle(object sender, MouseEventArgs e)
		{
			var pos = e.GetPosition(cvPoints);
			var radius = slRadius.Value / 2.0;
			Canvas.SetLeft(selection, pos.X - radius);
			Canvas.SetTop(selection, pos.Y - radius);
		}

		private void RadiusChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			selection.Width = e.NewValue;
			selection.Height = e.NewValue;
		}

		private void CanvasLeftClicked(object sender, MouseButtonEventArgs e)
		{
			var center = e.GetPosition(cvPoints);
			var squaredRadius = ((slRadius.Value / 2.0) * (slRadius.Value / 2.0));
			foreach (var mappedLed in leds)
			{
				var squaredDist = center.X - mappedLed.MappedPosition.X;
				squaredDist *= squaredDist;
				var dy = center.Y - mappedLed.MappedPosition.Y;
				squaredDist += dy * dy;
				if (squaredDist < squaredRadius)
				{
					var color = mappedLed.Led.Color;
					color.Brightness = (byte)slBrightness.Value;
					color.R = (byte) slRed.Value;
					color.G = (byte) slGreen.Value;
					color.B = (byte) slBlue.Value;
					mappedLed.Pixel.Fill = ToSCB(mappedLed.Led);
				}
			}
		}

		private void Preview(object sender, RoutedEventArgs e)
		{
			var activeLeds = leds.Where(x => x.Led.Color.Active).GroupBy(x => x.Led.ModuleId);
			var moduleFactory = new ModuleFactory();
			var communicators = moduleFactory.GetAllModules().ToDictionary(x => x.Id, x => new LedCommunicator(x));
			foreach (var moduleLeds in activeLeds)
			{
				var maxIndex = moduleLeds.Max(x => x.Led.Index);
				var data = new byte[maxIndex * 4];
				for (int i = 0; i < maxIndex; i++)
				{
					var led = moduleLeds.SingleOrDefault(x => x.Led.Index == i)?.Led;
					if (led != null)
					{
						Array.Copy(led.Color.ToRGBB(), 0, data, i * 4, 4);
					}
				}
				communicators[moduleLeds.Key].SetRGBB(data);
			}
		}

		private class MappedLed
		{
			public LedValue Led { get; set; }
			public Point MappedPosition { get; set; }
			public Rectangle Pixel { get; set; }
		}
	}
}
