﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataPersistance.Modules;

namespace Proto
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private LedController ledController;

		public MainWindow()
		{
			ledController = new LedController();
			InitializeComponent();
			LoadPresets();
		}

		private void OpenPresetCreator(object sender, RoutedEventArgs e)
		{
			var ps = new PresetSelector();
			ps.ShowDialog();
			LoadPresets();
		}

		private void LoadPresets()
		{
			cbPresets.Items.Clear();
			var presets = ledController.GetAllPresets();
			foreach (var preset in presets)
			{
				cbPresets.Items.Add(new ComboBoxItem {Content = preset});
			}
		}

		private void ApplySelection(object sender, RoutedEventArgs e)
		{
			throw new NotImplementedException();
		}
	}
}
