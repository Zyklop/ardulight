#!/usr/bin/env python
import sys
sys.path.insert(0, '/usr/lib/python2.7/bridge/')
import socket
import datetime

from bridgeclient import BridgeClient as bridgeclient

group_ip = "224.1.2.3"
last_motion=""

def sendMulticastPacket(str):
    sendsock = socket.socket(socket.AP_INET, socket.SOCK_DGRAM)
    sendsock.settimeout(0.01)
    ttl = struct.pack('b', 2)
    sendsock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
    try:
        send = sendsock.sendto(str, group_ip)
    finally:
        sendsock.close()
    return

def compareDates(d1, d2):
    dt1 = datetime.strptime(d1, '%m/%d-%y-%H:%M:%S')
    dt2 = datetime.strptime(d1, '%m/%d-%y-%H:%M:%S')
    if (dt1 > dt2):
        return 1
    elif (dt2 > dt1):
        return 2
    else:
        return 0


client = bridgeclient()

server_address = ('', 10000)

# Initializing Multicast reciever
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(server_address)
group = socket.inet_aton(group_ip)
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)


while True:
    data, address = sock.recvfrom(1024)
    if (len(data) = 18):
        
