#include <dht.h>
#include <Bridge.h>
#include <YunServer.h>
#include <YunClient.h>
#include <Console.h>
#include <HttpClient.h>
#include <Process.h>
#include <FileIO.h>
#include <Mailbox.h>

#define DEBUG true

// Listen on default port 5555, the webserver on the Yún
// will forward there all the HTTP requests for us.
YunServer server;
const byte PwmOutputs[] = {3,5,6,10,11};
byte values[5];
const byte MotionPin = 2;
const byte DhtPin = 7;
const byte dimmerPin = 0;
const byte airPin = 1;
const byte photoPin = 2;
const byte reedPin = 3;
const byte powerPin = 8;
const byte alarmPin = 12;
boolean pir = LOW;
boolean motion = LOW;
boolean on = LOW;
dht DHT;

void setup() {
  // Bridge startup
  int i;
  for (i = 0; i < (sizeof(PwmOutputs)/sizeof(byte)) - 1; i++) {
     pinMode(PwmOutputs[i], OUTPUT);
  }
  pinMode(MotionPin, INPUT);
  pinMode(13, OUTPUT);
  pinMode(powerPin, OUTPUT);
  pinMode(alarmPin, OUTPUT);
  digitalWrite(13, LOW);
  Bridge.begin();
  if(DEBUG) {
    Console.begin();
    while(!Console.connected()) {
      digitalWrite(13, HIGH);
      delay(20);
      digitalWrite(13, LOW);
    }
    int chk = DHT.read22(DhtPin);
    switch (chk)
    {
      case DHTLIB_OK:  
  		Console.print("OK,\t"); 
  		break;
      case DHTLIB_ERROR_CHECKSUM: 
  		Console.print("Checksum error,\t"); 
  		break;
      case DHTLIB_ERROR_TIMEOUT: 
  		Console.print("Time out error,\t"); 
  		break;
      default: 
  		Console.print("Unknown error,\t"); 
  		break;
    }
  } 
  else {
    int chk = DHT.read22(DhtPin);
    switch (chk)
    {
      case DHTLIB_OK:  
  		break;
      case DHTLIB_ERROR_CHECKSUM: 
  		break;
      case DHTLIB_ERROR_TIMEOUT: 
  		break;
      default:
  		break;
    }
  }
  //read config
  if (!FileSystem.begin()) {
    Console.println("error acessing Filesystem");
  }
  File dataFile = FileSystem.open("/mnt/sda1/arduino/values.txt", FILE_READ);
  unsigned char val;
  if (dataFile) {
    for (i=0; i < (sizeof(PwmOutputs)/sizeof(byte)) - 1; i++) {
      val = dataFile.read();
      values[i] = val;
   }
   if(DEBUG) {
      Console.println("Values successfully stored");
    }
    dataFile.close();
  }
  else if(DEBUG) {
    Console.println("error opening values.txt");
  } 
  digitalWrite(13, HIGH);

  // Listen for incoming connection only from localhost
  // (no one from the external network could connect)
  server.listenOnLocalhost();
  server.begin();
}

void loop() {
  int sensorValue;
  // Check for manual override
  sensorValue = analogRead(dimmerPin);
  if (sensorValue < 1000) {
    if (DEBUG) {
      Console.print("Dimmer set to: ");
      Console.print(sensorValue);
      Console.println(" ,overriding");
    }
    if(!on)
    {
      digitalWrite(powerPin, HIGH);
      on = HIGH;
      delay(10);
    }
    sensorValue = map(sensorValue, 0, 1023, 0, 255);
    int i;
    for (i=0; i < (sizeof(PwmOutputs)/sizeof(byte)) - 1; i++){
      analogWrite(PwmOutputs[i], sensorValue);
    }
  }
  else {
    // Read motion-sensor
    motion = digitalRead(MotionPin);
      if (motion == HIGH && pir == LOW) {
        if (DEBUG) {
          Console.println("Motion Detected");
        }
        MotionDetected(HIGH);
        pir = HIGH;
    }
    else if (motion == LOW && pir == HIGH){
      MotionDetected(LOW);
      pir = LOW;
    }
    // Get clients coming from server
    YunClient client = server.accept();
    // There is a new client?
    if (client) {
      // Process request
      process(client);
  
      // Close connection and free resources.
      client.stop();
    }
  }

  delay(10); // Poll every 10ms
}

void MotionDetected(boolean value) {
  Bridge.put("Motion", getTimeStamp());
  //FadeOn();
}

String getTimeStamp() {
  String result;
  Process time;
  time.begin("date");
  time.addParameter("+%D-%T");  
  time.run(); 

  while(time.available()>0) {
    char c = time.read();
    if(c != '\n')
      result += c;
  }

  return result;
}

void FadeOn() {
  byte m = 0; 
  if (!on) {
    digitalWrite(powerPin, HIGH);
    on = HIGH;
    delay(10);
  }
  for (byte i=0; i < (sizeof(PwmOutputs)/sizeof(byte)) - 1; i++) {
      m = max(m, values[i]);
   }
   for (byte i=1; i < m; i++) {
     for (byte j=0; i < (sizeof(PwmOutputs)/sizeof(byte)) - 1; j++) {
       if (values[j] > i) {
         digitalWrite(PwmOutputs[j], i);
       }
     }
     delay(5);
   }
}

void FadeOff() {
  byte m = 0; 
  for (byte i=0; i < (sizeof(PwmOutputs)/sizeof(byte)) - 1; i++) {
      m = max(m, values[i]);
   }
   for (byte i=m; i >= 0; i--) {
     for (byte j=0; i < (sizeof(PwmOutputs)/sizeof(byte)) - 1; j++) {
       if (values[j] > i) {
         digitalWrite(PwmOutputs[j], i);
       }
     }
     delay(5);
   }
    digitalWrite(powerPin, LOW);
    on = LOW;
}
   

void process(YunClient client) {
  // read the command
  String command = client.readStringUntil('/');
  command.trim();
  if(DEBUG)
  {
    Console.println("command detected: " + command);
  }
  // is "temp" command?
  if (command == "temp") {
    GetTemp(client);
  }
  // is "hum" command?
  if (command == "hum") {
    GetHum(client);
  }
  // is "air" command?
  if (command == "air") {
    GetAir(client);
  }
  // is "air" command?
  if (command == "photo") {
    GetPhoto(client);
  }
  // is "dim" command?
  if (command == "dim") {
    GetDimmerValue(client);
  }
  // is "reed" command?
  if (command == "reed") {
    GetReedValue(client);
  }
  // is "alarm" command?
  if (command == "alarm") {
    SetAlarm(client);
  }
  // is "motion" command?
  if (command == "motion") {
    GetDetectedMotion(client);
  }
  // is "light" command?
  if (command == "previewValue") {
    SetPwmOut(client);
  }
  // is "light" command?
  if (command == "setValue") {
    SaveAndSetOut(client);
  }
  // is "time" command?
  if (command == "time") {
    GetTime(client);
  }
  // is "turnOff" command?
  if (command == "turnOff") {
    TurnOff(client);
  }
  // is "turnOn" command?
  if (command == "turnOn") {
    TurnOn(client);
  }
  // is "Picture" command?
  if (command == "picture") {
    Picture(client);
  }
  // is "Picture" command?
  if (command == "isOn") {
    IsOn(client);
  }
}

void GetTemp(YunClient client) {
  client.println(DHT.temperature);
}

void GetHum(YunClient client) {
  client.println(DHT.humidity);
}

void GetAir(YunClient client) {
  client.println(analogRead(airPin));
}

void GetPhoto(YunClient client) {
  client.println(analogRead(photoPin));
}

void GetDimmerValue(YunClient client) {
  client.println(analogRead(dimmerPin));
}

void GetReedValue(YunClient client) {
  client.println(analogRead(ReedPin));
}

void SetAlarm(YunClient client) {
  int freq = client.parseInt();
  if(freq == 0)
  {
    noTone(alarmPin);
  }
  else
  {
    tone(alarmPin, freq);
  }
}

void GetDetectedMotion(YunClient client) {
}

void SetPwmOut(YunClient client) {
  int pin = client.parseInt();
  // If the next character is a '/' it means we have an URL
  // with a value like: "/digital/13/1"
  if (client.read() == '/') {
    int value = client.parseInt();
    digitalWrite(PwmOutputs[pin], value);
    client.println("ok");
  }
  else {
    client.println("Could not parse Values");
  }  
}

void SaveAndSetOut(YunClient client) {
  int pin = client.parseInt();
  // If the next character is a '/' it means we have an URL
  // with a value like: "/digital/13/1"
  if (client.read() == '/') {
    int value = client.parseInt();
    values[pin] = value;
    SaveValues();
    digitalWrite(PwmOutputs[pin], value);
    client.println("ok");
  }
  else {
    client.println("Could not parse Values");
  }  
}

void SaveValues() {
  if (!FileSystem.begin()) {
    Console.println("error acessing Filesystem");
  }
  File dataFile = FileSystem.open("/mnt/sda1/arduino/values.txt", FILE_WRITE);
  unsigned char val;
  if (dataFile) {
    dataFile.write(values, sizeof(values)/sizeof(byte));
   if(DEBUG) {
      Console.println("Values successfully stored");
    }
    dataFile.close();
  }
  else if(DEBUG) {
    Console.println("error opening values.txt");
  }
}

void GetTime(YunClient client) {
  client.println(getTimeStamp());
}

void TurnOff(YunClient client) {
  FadeOff();
}

void TurnOn(YunClient client) {
  FadeOn();
}

void IsOn(YunClient client) {
  client.println(on);
}

void Picture(YunClient client) {
  Process webcam;
  String fn = getTimeStamp();
  fn.replace(' ', '_');
  fn.replace('.', '_');
  fn = "img_" + fn + ".jpg";
  webcam.begin("fswebcam");
  webcam.addParameter("-i 0");
  webcam.addParameter("--no-banner");
  webcam.addParameter("--jpeg 95");
  webcam.addParameter("--save /mnt/sda1/arduino/www/camera/" + fn);
  webcam.run();
  delay(3000);
  client.println("../sd/camera/" + fn);
}